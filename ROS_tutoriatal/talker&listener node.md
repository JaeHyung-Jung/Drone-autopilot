* 수정한 것들
- package.xml(depend 추가)
- CMakelists.txt(~.cpp ~.cpp (pub, sub) include )

* 생성한 파일
- talker.cpp
- listener.cpp

* Directory 구조 ( +, - : 폴더, . : 파일)
pwd : home/user/catkin_ws/src
- begginer_tutorial
   + include
   + msg
   - src
      . listener.cpp
      . talker.cpp
   + srv
   . CMakeLists.txt
   . Package.xml
   
* 오류가 났었던 부분 :
catkin_make가 안됐으며 원인은 package.xml에서 message_runtime에 대한 depend가 주석처리 돼있었음

* 실행
- $ rosrun beginner_tutorials talker => 설정한 loop값에 맞게 메시지 출력
- $ rosrun beginner_tutorials listener => talker(publisher node)에서 전송한 msg를 받음
