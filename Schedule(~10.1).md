SierraBASE 정재형 스케쥴(seriously very important, ㅈㄴ중요함 꼭 기한 지켜야됨)

6.27(월) ~ 6.30(목) :
 - ROS 강의 모두 시청
07.01(금)  : 
 - ROS강의 개념 정리 후 발표(keyword 위주로) 
  => : 발표(ROS 개념 keyword, 공부한것들, 드론 기본세팅 및 Simulation(8월까지 해야되는것) 스케쥴 알리기)

07.01(금) ~ 08.01(월) : 
1) Drone 기본세팅(Xavier 설치, TF만들기, 자동실행, Usb port 고정, Xavier USB 속도제한 해제, Lidar 기본세팅, IMU세팅) 
   + Xavier 초기세팅(SDK, Ros pkg, MavROS, Ardupilot, Opencv build)
1-1) Catkin create pkg + (image input.cpp, manipulation(navigation).cpp 짜서 cmake)

2) - Simulation 구성(ROS, Ardupilot, Xavier(Opencv), Airsi192.1m)
    - TF구성 ( URDF[depth, stereo, lidar, IMU, gps] + Camera Module 추가 )
    - Joint Link + Joint State publisher
    - map + baseLink
    - Lidar 처음부터 다시 구성

8.1(월) ~ 10.1 :
    - pkg 내의 src에서 .cpp직접 짜면서 cmake
    - SLAM 종류별 공부(Lom, Lego, (vision&Lidar SLAM) 논문 및 코드분석)
    - 최종적으로 Drone Simulator + SLAM 동작가능하게 10월말까지 해야함

SLAM jotbob gitlab에 SLAM짠거 업로드
